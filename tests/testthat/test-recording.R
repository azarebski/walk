context("Speed of data structures for recording samples")

#' Based on the tests in this file it seems that if we construct the
test_that("Preallocation is quick than growing a vector", {

    test_length <- 3e3
    test_width <- 11

    growing <- function() {
        a <- rnorm(n = test_width)
        temp <- a
        for (i in 2:test_length) {
            temp <- rnorm(n = test_width, mean = temp)
            a <- c(a, temp)
        }
        b <- matrix(a, nrow = test_length, ncol = test_width)
        mean(b)
    }
    elapsed_growing <- system.time(growing())["elapsed"]

    alloc_v_1 <- function() {
        a <- vector(mode = "numeric", length = test_length * test_width)
        temp <- rnorm(n = test_width)
        for (i in 2:test_length) {
            temp <- rnorm(n = test_width, mean = temp)
            a[((i - 1) * test_width) + 1:test_width] <- temp
        }
        b <- matrix(a, nrow = test_length, ncol = test_width)
        b
    }
    elapsed_alloc_v_1 <- system.time(alloc_v_1())["elapsed"]

    alloc_v_2 <- function() {
        a <- matrix(nrow = test_length, ncol = test_width)
        temp <- rnorm(n = test_width)
        for (i in 2:test_length) {
            temp <- rnorm(n = test_width, mean = temp)
            a[i,] <- temp
        }
        a
    }
    elapsed_alloc_v_2 <- system.time(alloc_v_2())["elapsed"]

    av3_helper <- function(temp, b) {
        return(rnorm(n = test_width, mean = temp))
    }

    alloc_v_3 <- function() {
        a <- Reduce(f = av3_helper, x = 2:test_length, init = rnorm(n = test_width), accumulate = TRUE)
        b <- do.call(rbind, a)
        return(b)
    }
    elapsed_alloc_v_3 <- system.time(alloc_v_3())["elapsed"]

    expect_lt(elapsed_alloc_v_1, 0.5 * elapsed_growing)
    expect_lt(elapsed_alloc_v_2, 0.8 * elapsed_alloc_v_1)
    expect_equal(elapsed_alloc_v_2, elapsed_alloc_v_3, tol = 1e-1)
})
