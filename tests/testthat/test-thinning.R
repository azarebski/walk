context("Chain thinning")

test_that("Thinning works", {
    set.seed(1)
    target_density <- function(theta) {
        x <- unlist(theta)
        radius <- sqrt(sum(x ^ 2))
        dgamma(radius, 10, 5)
    }

    random_walk_candidate_density <- function(curr_params, cand_params) {
        curr <- unlist(curr_params)
        cand <- unlist(cand_params)
        prod(dnorm(cand, mean = curr, sd = 1))
    }

    random_walk_sample <- function(curr, target_func) {
        parameters <- list(coord1 = rnorm(1, mean = curr$coord1, sd = 1),
                           coord2 = rnorm(1, mean = curr$coord2, sd = 1))
        target_val <- target_func(parameters)
        list(params = parameters,
             target = target_val)
    }

    random_step <- function(curr) {
        random_walk_sample(curr, target_density)
    }

    random_initial_condition <- function(target_func) {
        parameters <- list(coord1 = runif(1), coord2 = runif(1))
        target_val <- target_func(parameters)
        list(params = parameters,
             target = target_val)
    }

    random_ic <- function() {
        random_initial_condition(target_density)
    }

    run_sampler <- function(thinning, num_iters, num_burn) {
        RAND_mcmc_chains(random_step,
                         target_density,
                         random_walk_candidate_density,
                         random_ic,
                         num_iters = num_iters,
                         num_burn = num_burn,
                         thinning = thinning,
                         num_chains = 3)
    }

    samples_thin <- run_sampler(10, 1100, 100)
    samples_medium <- run_sampler(5, 700, 200)
    samples_thick <- run_sampler(1, 100, 0)
    samples_thick_slow <- run_sampler(1, 500, 300)
    samples_weird <- run_sampler(3, 8, 4)

    expect_equal(coda::niter(samples_thin$mcmc.list), 100)
    expect_equal(coda::thin(samples_thin$mcmc.list), 10)
    expect_equal(coda::niter(samples_medium$mcmc.list), 100)
    expect_equal(coda::thin(samples_medium$mcmc.list), 5)
    expect_equal(coda::niter(samples_thick$mcmc.list), 100)
    expect_equal(coda::thin(samples_thick$mcmc.list), 1)

    ess_comp <- (coda::effectiveSize(samples_thin$mcmc.list) >
                 coda::effectiveSize(samples_medium$mcmc.list))
    expect_equal(sum(ess_comp), length(ess_comp))

    ess_comp <- (coda::effectiveSize(samples_medium$mcmc.list) >
                 coda::effectiveSize(samples_thick$mcmc.list))
    expect_equal(sum(ess_comp), length(ess_comp))

    psrf_comp <- ( (coda::gelman.diag(samples_thick_slow$mcmc.list)$psrf) <
                  (coda::gelman.diag(samples_thick$mcmc.list)$psrf))
    expect_equal(sum(psrf_comp), length(psrf_comp))

    ## 8 samples.
    ## dropping the first 4 leaves 4.
    ## take sample 1 then skip 3.
    ## total of 1 sample
    expect_equal(coda::niter(samples_weird$mcmc.list), 1)
})
